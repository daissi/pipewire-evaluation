#!/usr/bin/make -f

all:
	pandoc -f markdown-implicit_figures README.md -o AT10085_report.pdf
	pandoc README.md -o AT10085_report.html
	pandoc -f markdown-implicit_figures README_v2024.md -o AT10170_report.pdf
	pandoc README_v2024.md -o AT10170_report.html
