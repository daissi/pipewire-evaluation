set output 'Timing5.svg
set terminal svg
set multiplot
set grid
set title "Clients duration"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 10 title "jackplay/83" with lines, "profiler-test-case-5.log" using 18 title "output.pw-loopback-742/80" with lines, "profiler-test-case-5.log" using 26 title "my-sink-A/81" with lines, "profiler-test-case-5.log" using 34 title "jackplay/100" with lines, "profiler-test-case-5.log" using 42 title "output.pw-loopback-795/87" with lines, "profiler-test-case-5.log" using 50 title "my-sink-B/86" with lines, "profiler-test-case-5.log" using 58 title "alsa_output.platform-sound.stereo-fallback/32" with lines
unset multiplot
unset output
