set output 'Timing3.svg
set terminal svg
set multiplot
set grid
set title "Clients end date"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 1 title "Audio period" with lines, "profiler-test-case-5.log" using 8 title "jackplay/83" with lines, "profiler-test-case-5.log" using 16 title "output.pw-loopback-742/80" with lines, "profiler-test-case-5.log" using 24 title "my-sink-A/81" with lines, "profiler-test-case-5.log" using 32 title "jackplay/100" with lines, "profiler-test-case-5.log" using 40 title "output.pw-loopback-795/87" with lines, "profiler-test-case-5.log" using 48 title "my-sink-B/86" with lines, "profiler-test-case-5.log" using 56 title "alsa_output.platform-sound.stereo-fallback/32" with lines
unset multiplot
unset output
