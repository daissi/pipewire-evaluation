set output 'Timing1.svg
set terminal svg
set multiplot
set grid
set title "Audio driver timing"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 3 title "Audio driver delay" with lines, "profiler-test-case-5.log" using 1 title "Audio period" with lines,"profiler-test-case-5.log" using 4 title "Audio estimated" with lines
unset multiplot
unset output
