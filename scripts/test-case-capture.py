#!/usr/bin/env python3

import json
import os
import signal
import subprocess


def pw_dump(name):
    cmd_pw_dump = ["pw-dump"]
    p = subprocess.run(cmd_pw_dump, stdout=subprocess.PIPE)
    my_pw_dump = json.loads(p.stdout)

    with open(name, "w") as f:
      json.dump(my_pw_dump, f, indent=2)

    return my_pw_dump


def pw_record(node, file):
    # FIXME normalize volume of LINEIN before...
    cmd_pw_link_delete = ["pw-record", "--volume", "1", "--channels", "2",
                          "--quality", "15",
                          "--target", node, file]
    pw_rec = subprocess.Popen(cmd_pw_link_delete, preexec_fn=os.setsid)
    return pw_rec


if __name__ == "__main__":
    print("Running capture for test-case")

    my_pw_dump = pw_dump("pw-dump-temp-capture.json")

    my_nodes_name = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if "api.alsa.pcm.stream" not in pw_obj["info"]["props"]:
        continue
      if pw_obj["info"]["props"]["api.alsa.pcm.stream"] != "capture":
        continue
      # ICUSBAUDIO7D is the card name as displayed by "aplay -l"
      if pw_obj["info"]["props"]["api.alsa.card.name"] != "ICUSBAUDIO7D":
        continue
      my_nodes_name.append(pw_obj["info"]["props"]["node.name"])

    print(f"Start capture for pw nodes:\n  {my_nodes_name}")

    pw_rec1 = pw_record(my_nodes_name[0], "test-case-capture1.wav")
    pw_rec2 = pw_record(my_nodes_name[1], "test-case-capture2.wav")

    print("Press any key to stop the capture...")
    input()
    os.killpg(os.getpgid(pw_rec1.pid), signal.SIGTERM)
    os.killpg(os.getpgid(pw_rec2.pid), signal.SIGTERM)
    print("End of capture for test-case")
