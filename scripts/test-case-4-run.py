#!/usr/bin/env python3

from statistics import mean
import matplotlib.pyplot as plt
import json
import os
import signal
import subprocess
import time

def pw_dump(name):
    cmd_pw_dump = ["pw-dump"]
    p = subprocess.run(cmd_pw_dump, stdout=subprocess.PIPE)
    my_pw_dump = json.loads(p.stdout)

    with open(name, "w") as f:
      json.dump(my_pw_dump, f, indent=2)

    return my_pw_dump


def wpctl_set_default(dev_id):
    cmd_wpctl_set_default = ["wpctl", "set-default", str(dev_id)]
    p = subprocess.run(cmd_wpctl_set_default, stdout=subprocess.PIPE)


def set_default_device(device_name):
    my_pw_dump = pw_dump("pw-dump.json")

    my_device_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if "api.alsa.pcm.stream" not in pw_obj["info"]["props"]:
        continue
      if pw_obj["info"]["props"]["api.alsa.pcm.stream"] != "playback":
        continue
      # ICUSBAUDIO7D is the card name as displayed by "aplay -l"
      if pw_obj["info"]["props"]["api.alsa.card.name"] != device_name:
        continue
      my_device_id.append(pw_obj["id"])

    print(f"Setting {device_name} id {my_device_id[0]} as default playback device")
    wpctl_set_default(my_device_id[0])

    return my_device_id


def start_jackplay(my_sound):
    cmd_jackplay = [
      "pw-jack",
      "sndfile-jackplay",
      my_sound
    ]
    subprocess.run(cmd_jackplay, stderr=subprocess.DEVNULL)

def start_jackplay_loop(my_sound):
    cmd_jackplay = [
      "pw-jack",
      "sndfile-jackplay",
      my_sound,
      "-l",
      "0"
    ]
    subprocess.Popen(cmd_jackplay, stderr=subprocess.DEVNULL)

def kill_my_sinks():
    print("Killing all sndfile-jackplay")
    subprocess.run(["killall","sndfile-jackplay"],
                   stdout = subprocess.DEVNULL,
                   stderr = subprocess.DEVNULL)


if __name__ == "__main__":
    print("Run test-case 4")

    my_device_id = set_default_device("ICUSBAUDIO7D")
    my_wav_loop = "test300.wav"
    my_wav = "/usr/share/sounds/alsa/Front_Center.wav" # duration of 1.428 sec, checked with Audacity

    # Generate different batches of sndfile-jackplay
    my_batches = list(range(1, 60, 10))
    my_results = {new_list: [] for new_list in my_batches}
    for nb_play in my_batches:
      for run in range(1,11):
        # To avoid interference from previous run
        kill_my_sinks()
        time.sleep(2) # Wait 2 sec to be sure all sndfile-jackplay are killed

        print(f"\nStarting {nb_play} sndfile-jackplay")
        start_jackplay_loop(my_wav_loop)
        for nb_play_i in list(range(2, nb_play)):
          start_jackplay_loop(my_wav_loop)
        time.sleep(2) # Wait 2 sec to be sure all sndfile-jackplay are running
        print("Everything is ready for testing!")
        print("A new sndfile-jackplay will be run and time required will be measured")
        #print("Press any key to start the test...")
        #input()

        t_start = time.time()
        start_jackplay(my_wav)
        t_end = time.time()
        required_time = t_end - t_start
        my_results[nb_play].append(required_time)

        print(f"With {nb_play} running, it tooks {required_time} " +
               "to connect and play the sound")

      kill_my_sinks()


    with open(r'test-case-4-list-results.txt', 'w') as f:
      for k, v in my_results.items():
        f.write(f'{k}: {v}\n')

    # Generate a graph of required_time
    x=list(my_results.keys())
    y=[]
    for batch in my_batches:
      y.append(mean(my_results[batch]))
    plt.plot(x,y, color="red", label="mean of measurements")
    plt.xlabel('Already running sndfile-jackplay')
    plt.ylabel('Time to play a new sound')
    plt.title("Test case 4")
    plt.savefig('test-case-4-plot-mean.png')
    with open(r'test-case-4-list-means.txt', 'w') as f:
      for item in y:
        f.write("%s\n" % item)

    plt.figure()
    for batch in my_batches:
      for yi in range(len(my_results[batch])):
        plt.scatter(x=batch,y=my_results[batch][yi], c="blue", alpha=0.5)
    plt.plot(x,y, color="red", label="mean of measurements")
    plt.xlabel('Already running sndfile-jackplay')
    plt.ylabel('Time to play a new sound')
    plt.title("Test case 4")
    plt.savefig('test-case-4-plot.png')

    # End of test case 4
    print("End of test-case 4")
