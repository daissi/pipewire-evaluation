#!/usr/bin/env python3

import json
import os
import signal
import subprocess
import sys
import time


def pw_dump(name = None):
    cmd_pw_dump = ["pw-dump"]
    p = subprocess.run(cmd_pw_dump, stdout=subprocess.PIPE)
    my_pw_dump = json.loads(p.stdout)

    if name != None:
        with open(name, "w") as f:
            json.dump(my_pw_dump, f, indent=2)

    return my_pw_dump


def pw_dot(name):
    subprocess.run(["pw-dot","-o","pw-dot-temp.dot"], stderr=subprocess.DEVNULL)
    subprocess.run(["dot","-Tpng","pw-dot-temp.dot","-o",name], stderr=subprocess.DEVNULL)


def pw_profiler(output_name="profiler-test-case-5.log"):
    pw_prof_id = subprocess.Popen(["pw-profiler","-o",output_name],
                                   stdout=subprocess.DEVNULL,
                                   stderr=subprocess.DEVNULL,
                                   preexec_fn=os.setsid
                                  )
    return pw_prof_id


def dump_pw_top(output_name="pw-top.out"):
    # pipewire in apertis/v2023 doesn't support "pw-top  --batch-mode"
    # This is a workaround to save the output of pw-top
    #   echo | pw-top  > pw-top.out
    #   head -c -10 pw-top.out | tail -c +10

    cmd_dump_pw_top = "echo | pw-top  > "+output_name
    p = subprocess.Popen(cmd_dump_pw_top, preexec_fn=os.setsid, shell=True)
    time.sleep(2)
    os.killpg(os.getpgid(p.pid), signal.SIGTERM)


def dump_pw_top_batch(output_name="pw-top.log"):
    cmd_dump_pw_top = ["pw-top","--batch-mode","--iterations","10"]
    with open(output_name, "w") as outfile:
        subprocess.run(cmd_dump_pw_top, stdout=outfile)


def pw_link(out_pattern, in_pattern):
    cmd_pw_link = ["pw-link",str(out_pattern),str(in_pattern)]
    subprocess.run(cmd_pw_link)


def pw_link_delete(out_pattern, in_pattern):
    cmd_pw_link_delete = ["pw-link","-d",str(out_pattern),str(in_pattern)]
    subprocess.run(cmd_pw_link_delete)


def pw_metadata(out_pattern, in_pattern):
    cmd_pw_metadata = ["pw-metadata", str(out_pattern),
                       "target.object", in_pattern]
    subprocess.run(cmd_pw_metadata,
                   stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def create_loopback_device(name):
    print(f"Creating a virtual device named {name}")

    cmd_pw_loopback = "pw-loopback -m '[FL FR]' --capture-props='media.class=Audio/Sink node.name="+name+"'"
    subprocess.Popen(cmd_pw_loopback, shell=True)


def start_jackplay(my_sound):
    print(f"Starting playing music with jackplay")

    cmd_jackplay = [
      "pw-jack",
      "sndfile-jackplay",
      my_sound,
      "-l",
      "0"
    ]
    subprocess.Popen(cmd_jackplay, stderr=subprocess.DEVNULL)


def delete_useless_pw_links(my_pw_dump, output_ports, input_ports):
    my_useless_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Link":
        continue
      if pw_obj["info"]["output-port-id"] not in output_ports:
        continue
      if pw_obj["info"]["input-port-id"] in input_ports:
        continue
      print(f"Destroying link between {pw_obj['info']['output-port-id']} and {pw_obj['info']['input-port-id']}")
      pw_link_delete(pw_obj["info"]["output-port-id"],
                     pw_obj["info"]["input-port-id"])


def kill_my_sinks():
    print("Killing all sndfile-jackplay and pw-loopback")
    subprocess.run(["killall","sndfile-jackplay"], stderr=subprocess.DEVNULL)
    subprocess.run(["killall","pw-loopback"], stderr=subprocess.DEVNULL)


def start_sink(my_sink_name, my_wav, device_playback, my_id_used=None):
    create_loopback_device(my_sink_name)
    start_jackplay(my_wav)
    time.sleep(5) # Wait 5 sec to be sure it started

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-1.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-1.png")

    # Get node id of "jackplay"
    my_jackplay_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != "jackplay":
        continue
      if my_id_used:
        if pw_obj["id"] in my_id_used:
          continue
      my_jackplay_id.append(pw_obj["id"])

    # Get associated ports id of "jackplay"
    my_jackplay_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Port":
        continue
      if pw_obj["info"]["props"]["node.id"] != my_jackplay_id[0]:
        continue
      my_jackplay_ports_id.append(pw_obj["id"])
    print(f".. DBG: my_jackplay_ports_id {my_jackplay_ports_id}")

    # Get node id of my_sink_name
    my_sink_id = []
    my_sink_node_group = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != my_sink_name:
        continue
      my_sink_id.append(pw_obj["id"])
      my_sink_node_group.append(pw_obj["info"]["props"]["node.group"])
    print(f".. DBG: my_sink_id {my_sink_id}")
    print(f".. DBG: my_sink_node_group {my_sink_node_group}")

    # Get associated ports id of my_sink_name
    my_sink_object_path = my_sink_node_group[0]+":playback"
    print(f".. DBG: my_sink_object_path {my_sink_object_path}")
    my_sink_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Port":
        continue
      if "port.alias" not in pw_obj["info"]["props"]:
        continue
      if not my_sink_object_path in pw_obj["info"]["props"]["port.alias"]:
        continue
      my_sink_ports_id.append(pw_obj["id"])
    print(f".. DBG: my_sink_ports_id {my_sink_ports_id}")

    # Move stream to the virtual node
    pw_link(my_jackplay_ports_id[0], my_sink_ports_id[0])
    pw_link(my_jackplay_ports_id[1], my_sink_ports_id[1])

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-2.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-2.png")

    # Get id of virtual node
    my_sink_node_group = None
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["id"] != my_sink_id[0]:
        continue
      my_sink_node_group = pw_obj["info"]["props"]["node.group"]

    my_loopback_output = None
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != ("output."+my_sink_node_group):
        continue
      my_loopback_output = pw_obj['id']

    # Assign virtual node to device_playback
    print(f"Send sound to {device_playback}")
    pw_metadata(my_loopback_output, device_playback)

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-3.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-3.png")

    # Delete previous links
    delete_useless_pw_links(my_pw_dump, my_jackplay_ports_id, my_sink_ports_id)

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-4.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-4.png")

    # This is required to identify which jackplay are already processsed
    return my_jackplay_id


def stress_ng(stress_timeout="1m", parallel_tests=20):
    start_time = time.strftime("%H:%M:%S", time.localtime())
    print(f"Starting to simulate high CPU load at {start_time} for {stress_timeout}")
    cmd_stress_ng = ["stress-ng", "--timeout", stress_timeout,
                     "--class", "cpu",
                     "--all", str(parallel_tests),
                    ]
    subprocess.run(cmd_stress_ng)
    end_time = time.strftime("%H:%M:%S", time.localtime())
    print(f"End of stressing at {end_time}")


if __name__ == "__main__":
    print("Run test-case 5")

    # To avoid interference from previous run
    kill_my_sinks()

    my_sound_cards = ["alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo",
                      "alsa_output.platform-sound.stereo-fallback"]

    # Make sound with USB sound card
    my_jackplay_id_used_A = start_sink("my-sink-A", "test300.wav", my_sound_cards[0])

    # Make sound with onboard card
    my_jackplay_id_used_B = start_sink("my-sink-B", "test300.wav", my_sound_cards[1], my_jackplay_id_used_A)

    # Simulate high system load
    print("\nEverything is ready for capture!")
    print("System load will increase")
    print("Press any key to start the test...")
    print("... then press any key to stop the test")
    input()

    dump_pw_top_batch("pw-top-1.log")
    pw_prof_id = pw_profiler()
    stress_ng()
    os.killpg(os.getpgid(pw_prof_id.pid), signal.SIGTERM)
    dump_pw_top_batch("pw-top-2.log")

    # End of test case 5
    print("Press any key to stop the playback...")
    input()
    kill_my_sinks()
