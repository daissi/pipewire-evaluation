#!/bin/sh

# Decrease timeout to suspend device from 5secs to 1sec

sudo mkdir -p /etc/wireplumber/main.lua.d/

sudo cp -a /usr/share/wireplumber/main.lua.d/50-alsa-config.lua \
                 /etc/wireplumber/main.lua.d/50-alsa-config.lua

sudo sed -i 's/--\["session.suspend-timeout-seconds"\] = 5/--\["session.suspend-timeout-seconds"\] = 1/g' /etc/wireplumber/main.lua.d/50-alsa-config.lua

systemctl restart --user wireplumber.service
