#!/bin/sh

pw-dot -o pw-dot-temp.dot && dot -Tpng pw-dot-temp.dot -o test0.png

pw-loopback -m '[ FL FR ]' --capture-props='media.class=Audio/Sink node.name=output_builtin' --playback-props='target.object="alsa_output.platform-sound.stereo-fallback" node.name=output_builtin' &
pw-loopback -m '[ FL FR ]' --capture-props='media.class=Audio/Sink node.name=output_usb' --playback-props='target.object="alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo" node.name=output_usb' &

pw-dot -o pw-dot-temp.dot && dot -Tpng pw-dot-temp.dot -o test1.png

# Redirect input 1 to LEFT side of both output
pw-link input_1:output_FL output_builtin:playback_FL
pw-link input_1:output_FL output_usb:playback_FL

pw-link input_1:output_FR output_builtin:playback_FL
pw-link input_1:output_FR output_usb:playback_FL

## Drop previous direct links
pw-link -d input_1:output_FL alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo:playback_FL
pw-link -d input_1:output_FL alsa_output.platform-sound.stereo-fallback:playback_FL
pw-link -d input_1:output_FR alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo:playback_FL
pw-link -d input_1:output_FR alsa_output.platform-sound.stereo-fallback:playback_FL

pw-dot -o pw-dot-temp.dot && dot -Tpng pw-dot-temp.dot -o test2.png

# Redirect input 2 to RIGHT side of both output
pw-link input_2:output_FL output_builtin:playback_FR
pw-link input_2:output_FL output_usb:playback_FR

pw-link input_2:output_FR output_builtin:playback_FR
pw-link input_2:output_FR output_usb:playback_FR

## Drop previous direct links
pw-link -d input_2:output_FL alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo:playback_FR
pw-link -d input_2:output_FL alsa_output.platform-sound.stereo-fallback:playback_FR
pw-link -d input_2:output_FR alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo:playback_FR
pw-link -d input_2:output_FR alsa_output.platform-sound.stereo-fallback:playback_FR

pw-dot -o pw-dot-temp.dot && dot -Tpng pw-dot-temp.dot -o test3.png

