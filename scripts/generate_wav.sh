#!/bin/sh

# Generate two wav files with two different tone for 10 secs
gst-launch-1.0 audiotestsrc  freq=800 samplesperbuffer=44100   num-buffers=10  ! audio/x-raw,rate=44100,channels=2 ! wavenc ! filesink location=test800.wav
gst-launch-1.0 audiotestsrc  freq=300 samplesperbuffer=44100   num-buffers=10  ! audio/x-raw,rate=44100,channels=2 ! wavenc ! filesink location=test300.wav

# Generate a wav file for 2 secs
gst-launch-1.0 audiotestsrc  freq=300 samplesperbuffer=44100   num-buffers=2  ! audio/x-raw,rate=44100,channels=2 ! wavenc ! filesink location=test300_2s.wav
