#!/usr/bin/env python3

#from pwevaltools import *

from itertools import cycle
import json
import select
import subprocess
import sys
import time


def pw_dump(name):
    cmd_pw_dump = ["pw-dump"]
    p = subprocess.run(cmd_pw_dump, stdout=subprocess.PIPE)
    my_pw_dump = json.loads(p.stdout)

    with open(name, "w") as f:
      json.dump(my_pw_dump, f, indent=2)

    return my_pw_dump


def pw_dot(name):
    subprocess.run(["pw-dot","-o","pw-dot-temp.dot"], stderr=subprocess.DEVNULL)
    subprocess.run(["dot","-Tpng","pw-dot-temp.dot","-o",name], stderr=subprocess.DEVNULL)


def pw_cli_rate(device_id, rate):
    cmd_pw_cli = "pw-cli s "+str(device_id)+" Props '{ params: [ \"audio.rate\", \""+str(rate)+"\" ] }'"
    print(f".. DBG: {cmd_pw_cli}")
    subprocess.run(cmd_pw_cli, shell=True)


def pw_link(out_pattern, in_pattern):
    cmd_pw_link = ["pw-link",str(out_pattern),str(in_pattern)]
    subprocess.run(cmd_pw_link)


def pw_link_delete(out_pattern, in_pattern):
    cmd_pw_link_delete = ["pw-link","-d",str(out_pattern),str(in_pattern)]
    subprocess.run(cmd_pw_link_delete)


def pw_metadata(out_pattern, in_pattern):
    cmd_pw_metadata = ["pw-metadata", str(out_pattern),
                       "target.object", in_pattern]
    subprocess.run(cmd_pw_metadata,
                   stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def create_loopback_device(name):
    print(f"Creating a virtual device named {name}")

    cmd_pw_loopback = "pw-loopback -m '[FL FR]'"
    cmd_pw_loopback += " --capture-props='media.class=Audio/Sink node.name="+name+"'"
    #cmd_pw_loopback += " --playback-props='node.name="+name+"'"
    subprocess.Popen(cmd_pw_loopback, shell=True)


def create_loopback_mic_device(name):
    print(f"Creating a virtual device named {name}")

    cmd_pw_loopback = "pw-loopback -m '[FL FR]'"
    cmd_pw_loopback += " --capture-props='media.class=Audio/Sink node.name="+name+" target.object=alsa_input.usb-0d8c_USB_Sound_Device-00.analog-stereo'"
    cmd_pw_loopback += " --playback-props='node.name="+name+" target.object=alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo'"
    subprocess.Popen(cmd_pw_loopback, shell=True)
    time.sleep(2) # Wait for the node to be up


def start_jackplay(my_sound, my_group):
    print(f"Starting playing music with jackplay")

    cmd_jackplay = [
      "pw-jack",
      "sndfile-jackplay",
      my_sound,
      "-l",
      "0"
    ]

    cmd_jackplay_new = "PIPEWIRE_PROPS='{ node.group = "+my_group+" }' "
    cmd_jackplay_new += ' '.join(cmd_jackplay)

    subprocess.Popen(cmd_jackplay_new, shell=True, stderr=subprocess.DEVNULL)


def delete_useless_pw_links(my_pw_dump, output_ports, input_ports):
    my_useless_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Link":
        continue
      if pw_obj["info"]["output-port-id"] not in output_ports:
        continue
      if pw_obj["info"]["input-port-id"] in input_ports:
        continue
      print(f"Destroying link between {pw_obj['info']['output-port-id']} and {pw_obj['info']['input-port-id']}")
      pw_link_delete(pw_obj["info"]["output-port-id"],
                     pw_obj["info"]["input-port-id"])


def kill_my_sinks():
    print("Killing all sndfile-jackplay and pw-loopback")
    subprocess.run(["killall","sndfile-jackplay"], stderr=subprocess.DEVNULL)
    subprocess.run(["killall","pw-loopback"], stderr=subprocess.DEVNULL)


def start_sink(my_sink_name, my_wav, device_playback, my_id_used=None):
    create_loopback_device(my_sink_name)
    start_jackplay(my_wav, my_sink_name)
    time.sleep(5) # Wait 5 sec to be sure it started

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-1.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-1.png")

    # Get node id of "jackplay"
    my_jackplay_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != "jackplay":
        continue
      if my_id_used:
        if pw_obj["id"] in my_id_used:
          continue
      my_jackplay_id.append(pw_obj["id"])

    # Get associated ports id of "jackplay"
    my_jackplay_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Port":
        continue
      if pw_obj["info"]["props"]["node.id"] != my_jackplay_id[0]:
        continue
      my_jackplay_ports_id.append(pw_obj["id"])
    print(f".. DBG: my_jackplay_ports_id {my_jackplay_ports_id}")

    # Get node id of my_sink_name
    my_sink_id = []
    my_sink_node_group = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != my_sink_name:
        continue
      my_sink_id.append(pw_obj["id"])
      my_sink_node_group.append(pw_obj["info"]["props"]["node.group"])
    print(f".. DBG: my_sink_id {my_sink_id}")
    print(f".. DBG: my_sink_node_group {my_sink_node_group}")

    # Get associated ports id of my_sink_name
    my_sink_object_path = my_sink_node_group[0]+":playback"
    print(f".. DBG: my_sink_object_path {my_sink_object_path}")
    my_sink_ports_id = []
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Port":
        continue
      if "port.alias" not in pw_obj["info"]["props"]:
        continue
      if not my_sink_object_path in pw_obj["info"]["props"]["port.alias"]:
        continue
      my_sink_ports_id.append(pw_obj["id"])
    print(f".. DBG: my_sink_ports_id {my_sink_ports_id}")

    # Move stream to the virtual node
    pw_link(my_jackplay_ports_id[0], my_sink_ports_id[0])
    pw_link(my_jackplay_ports_id[1], my_sink_ports_id[1])

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-2.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-2.png")

    # Get id of virtual node
    my_sink_node_group = None
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["id"] != my_sink_id[0]:
        continue
      my_sink_node_group = pw_obj["info"]["props"]["node.group"]

    my_loopback_output = None
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if pw_obj["info"]["props"]["node.name"] != ("output."+my_sink_node_group):
        continue
      my_loopback_output = pw_obj['id']

    # Assign virtual node to device_playback
    print(f"Send sound to {device_playback}")
    pw_metadata(my_loopback_output, device_playback)

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-3.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-3.png")

    # Delete previous links
    delete_useless_pw_links(my_pw_dump, my_jackplay_ports_id, my_sink_ports_id)

    my_pw_dump = pw_dump("pw-dump-temp-"+my_sink_name+"-4.json")
    pw_dot("pw-dot-temp-"+my_sink_name+"-4.png")

    my_sink_node_group = "output."+my_sink_node_group

    # This is required to identify which jackplay are already processsed
    return my_jackplay_id, my_sink_node_group


def capture_mic_reconf(my_input_mic, node_to_close):
    my_pw_dump = pw_dump("pw-dump-temp-mic-0.json")
    my_capture_id = None
    for pw_obj in my_pw_dump:
      if pw_obj["type"] != "PipeWire:Interface:Node":
        continue
      if "api.alsa.pcm.stream" not in pw_obj["info"]["props"]:
        continue
      if pw_obj["info"]["props"]["api.alsa.pcm.stream"] != "capture":
        continue
      # alsa_output.platform-sound.stereo-fallback is the card name as displayed by "aplay -l"
      if pw_obj["info"]["props"]["node.name"] == my_input_mic:
        my_capture_id = pw_obj["id"]
        break

    # Due to hardware limitation we cannot directly capture at 8K or 16K Hz
    # Only 44100 and 48000 are supported
    # So, instead we will switch between 44K and 48K to capture "Line IN"
    my_supported_sample_rates = [44100, 48000]
    my_loopback_mic_name = "my_loopback_mic"
    create_loopback_mic_device(my_loopback_mic_name)
    pw_dot("pw-dot-temp-mic-0.png")

    print("\nEverything is ready for capturing MIC input!")
    print("Capture sample rate will change every 10 secs")
    print("Press any key to start the test...")
    print("... then press any key to stop the test")
    input()

    # Change sample rate every 10 secs
    for my_srate in cycle(my_supported_sample_rates):
      #pw_link_delete(my_input_mic, my_loopback_mic_name)
      pw_link_delete(my_input_mic+":capture_FL", my_loopback_mic_name+":playback_FL")
      pw_link_delete(my_input_mic+":capture_FR", my_loopback_mic_name+":playback_FR")

      # Need to wait until device is idle otherwise reconfigure is not applied,
      # Something to improve in pipewire? adding an option to force a device
      # reconfig without having to wait idle?
      time.sleep(5)

      print(f"Setting capture sample rate to {my_srate}")
      pw_cli_rate(my_capture_id, my_srate)

      #pw_link(my_input_mic, my_loopback_mic_name)
      pw_link(my_input_mic+":capture_FL", my_loopback_mic_name+":playback_FL")
      pw_link(my_input_mic+":capture_FR", my_loopback_mic_name+":playback_FR")

      # pw_dot("pw-dot-temp-mic-X.png")
      print(f"Please check sample rate with pw-link") # For DEBUG
      print(f"... sample rate for {my_input_mic} should be {my_srate}") # For DEBUG
      a, b, c = select.select([sys.stdin], [], [], 10)
      if (a):
        break


if __name__ == "__main__":
    print("Run test-case 2")

    # To avoid interference from previous run
    kill_my_sinks()

    my_sound_cards = ["alsa_output.usb-0d8c_USB_Sound_Device-00.analog-stereo",
                      "alsa_output.platform-sound.stereo-fallback"]

    my_input_mic = "alsa_input.usb-0d8c_USB_Sound_Device-00.analog-stereo"
    # Don't use the built-in card for capture due to hardware limitation
    # my_input_mic = "alsa_input.platform-sound.stereo-fallback"

    # Make sound with onboard card
    my_jackplay_id_used_A, my_jackplay_node_group_A = start_sink("my-sink-A", "test300.wav", my_sound_cards[1])

    # Make sound with USB sound card
    my_jackplay_id_used_B, my_jackplay_node_group_B = start_sink("my-sink-B", "test300.wav", my_sound_cards[0], my_jackplay_id_used_A)

    # Capture LINE IN on USB with audio input reconfiguration
    capture_mic_reconf(my_input_mic, my_jackplay_node_group_B)

    # End of test case 2
    print("Press any key to stop the playback...")
    input()
    kill_my_sinks()
