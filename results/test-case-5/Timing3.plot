set output 'Timing3.svg
set terminal svg
set multiplot
set grid
set title "Clients end date"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 1 title "Audio period" with lines, "profiler-test-case-5.log" using 8 title "output.pw-loopback-731/89" with lines, "profiler-test-case-5.log" using 16 title "my-sink-B/90" with lines, "profiler-test-case-5.log" using 24 title "jackplay/99" with lines
unset multiplot
unset output
