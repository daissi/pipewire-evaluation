set output 'Timing4.svg
set terminal svg
set multiplot
set grid
set title "Clients scheduling latency"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 9 title "output.pw-loopback-731/89" with lines, "profiler-test-case-5.log" using 17 title "my-sink-B/90" with lines, "profiler-test-case-5.log" using 25 title "jackplay/99" with lines
unset multiplot
unset output
