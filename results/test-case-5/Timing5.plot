set output 'Timing5.svg
set terminal svg
set multiplot
set grid
set title "Clients duration"
set xlabel "audio cycles"
set ylabel "usec"
plot "profiler-test-case-5.log" using 10 title "output.pw-loopback-731/89" with lines, "profiler-test-case-5.log" using 18 title "my-sink-B/90" with lines, "profiler-test-case-5.log" using 26 title "jackplay/99" with lines
unset multiplot
unset output
